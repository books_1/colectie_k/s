# S

## Content

```
./S. A. Chakraborty:
S. A. Chakraborty - Trilogia Daevabadului - V1 Orasul de bronz 1.0 '{BasmesiPovesti}.docx
S. A. Chakraborty - Trilogia Daevabadului - V2 Regatul de arama 1.0 '{BasmesiPovesti}.docx

./S. A. Cook:
S. A. Cook - Diamante pe luciul ghetii 0.99 '{Romance}.docx
S. A. Cook - Sot si sotie 0.8 '{Romance}.docx

./S. D. Perry:
S. D. Perry - Turnul lui Skeld 1.0 '{SF}.docx

./S. I. Mcmillen:
S. I. Mcmillen - Boli evitabile 0.8 '{Sanatate}.docx

./S. J. Watson:
S. J. Watson - Inainte sa adorm 1.0 '{Thriller}.docx
S. J. Watson - Second life 1.0 '{Thriller}.docx

./S. K. Tremayne:
S. K. Tremayne - Dubla identitate 1.0 '{Politista}.docx

./S. L. Jennings:
S. L. Jennings - Pervertirea 1.0 '{Erotic}.docx

./S. S. Van Dine:
S. S. Van Dine - Afacerea nebunul negru 0.9 '{Politista}.docx
S. S. Van Dine - O crima aproape perfecta 0.7 '{Politista}.docx

./S. Satrov:
S. Satrov - Blana de nylon 1.0 '{Umor}.docx

./Sabaa Tahir:
Sabaa Tahir - Elias si Spioana Carturarilor - V1 Focul din cenusa 1.0 '{SF}.docx
Sabaa Tahir - Elias si Spioana Carturarilor - V2 O torta in noapte 1.0 '{SF}.docx

./Sabina Wurmbrand:
Sabina Wurmbrand - Nobletea suferintei 0.8 '{Comunism}.docx

./Sabine Clark:
Sabine Clark - Dificultatile lui Kate 0.9 '{Romance}.docx
Sabine Clark - Un adversar pe masura 0.99 '{Dragoste}.docx

./Sabin Opreanu:
Sabin Opreanu - Insula din pergament 1.0 '{Versuri}.docx

./Sabrah Agee:
Sabrah Agee - Detectivul indragostit 1.0 '{Romance}.docx
Sabrah Agee - Intalnire de vis 1.0 '{Romance}.docx

./Sadie Turner:
Sadie Turner - Anomalii 1.0 '{SF}.docx

./Saint John Perse:
Saint John Perse - Poeme alese 0.99 '{Versuri}.docx

./Salla Simukka:
Salla Simukka - Alba ca Zapada - V1 Rosu ca sangele 1.0 '{Thriller}.docx
Salla Simukka - Alba ca Zapada - V2 Alb ca zapada 1.0 '{Thriller}.docx
Salla Simukka - Alba ca Zapada - V3 Negru ca abanosul 1.0 '{Thriller}.docx

./Sally Green:
Sally Green - Jumatate - V1 Jumatatea rea 1.0 '{SF}.docx
Sally Green - Jumatate - V2 Jumatatea salbatica 1.0 '{SF}.docx
Sally Green - Jumatate - V3 Jumatatea pierduta 1.0 '{SF}.docx

./Sally Trevor:
Sally Trevor - Pentru linistea ta 0.2 '{Dragoste}.docx

./Salman Rushdie:
Salman Rushdie - Copiii din miez de noapte 1.0 '{Literatura}.docx
Salman Rushdie - Furie 1.0 '{Literatura}.docx
Salman Rushdie - Grimus 0.8 '{Literatura}.docx
Salman Rushdie - Harun si marea de povesti 0.99 '{Literatura}.docx
Salman Rushdie - Luka si focul vietii 0.9 '{Literatura}.docx
Salman Rushdie - Rusinea 0.99 '{Literatura}.docx
Salman Rushdie - Seducatoarea din Florenta 0.4 '{Literatura}.docx
Salman Rushdie - Versetele satanice 2.0 '{Literatura}.docx

./Salvador Dali:
Salvador Dali - Jurnalul unui geniu 1.0 '{Biografie}.docx

./Salvator Dali:
Salvator Dali - Arta partului, elogiul mustei 0.99 '{Umor}.docx

./Salwa Al Neimi:
Salwa Al Neimi - Proba mierii 0.99 '{Erotic}.docx

./Samael Aun Weor:
Samael Aun Weor - Asana sacra practica 1.0 '{Spiritualitate}.docx
Samael Aun Weor - Cartea fecioarei din Carmel 1.0 '{Spiritualitate}.docx
Samael Aun Weor - Cartea galbena 1.0 '{Spiritualitate}.docx
Samael Aun Weor - Cartea mortilor 1.0 '{Spiritualitate}.docx
Samael Aun Weor - Casatoria perfecta 1.0 '{Spiritualitate}.docx
Samael Aun Weor - Cei trei munti 2.0 '{Spiritualitate}.docx
Samael Aun Weor - Cele 7 cuvinte 2.0 '{Spiritualitate}.docx
Samael Aun Weor - Cine este Samael Aun Weor 1.0 '{Spiritualitate}.docx
Samael Aun Weor - Constiinta Cristos 1.0 '{Spiritualitate}.docx
Samael Aun Weor - Da exista infern... da exista diavol... da exista karma 2.0 '{Spiritualitate}.docx
Samael Aun Weor - Desen Arbore Sephirotic 0.9 '{Spiritualitate}.docx
Samael Aun Weor - Dictionar esoteric vechi 1.0 '{Spiritualitate}.docx
Samael Aun Weor - Dincolo de moarte 1.0 '{Spiritualitate}.docx
Samael Aun Weor - Doctrina secreta din Anahuac 1.0 '{Spiritualitate}.docx
Samael Aun Weor - Gimnastica de lamaserie sau ritmuri tibetane 1.0 '{Spiritualitate}.docx
Samael Aun Weor - Gnoza Faza A 1.0 '{Spiritualitate}.docx
Samael Aun Weor - Gnoza Faza B 1.0 '{Spiritualitate}.docx
Samael Aun Weor - Initierea 1.0 '{Spiritualitate}.docx
Samael Aun Weor - Mantre 1.0 '{Spiritualitate}.docx
Samael Aun Weor - Marea rebeliune 2.0 '{Spiritualitate}.docx
Samael Aun Weor - Misterele focului 2.0 '{Spiritualitate}.docx
Samael Aun Weor - Mistere majore 1.0 '{Spiritualitate}.docx
Samael Aun Weor - Notiuni fundamentale de endocrinologie 1.0 '{Spiritualitate}.docx
Samael Aun Weor - Pistis Sophia dezvaluita 1.0 '{Spiritualitate}.docx
Samael Aun Weor - Psihologie revolutionara 1.0 '{Spiritualitate}.docx
Samael Aun Weor - Revolutia lui Bel 1.0 '{Spiritualitate}.docx
Samael Aun Weor - Simbologie onirica 2.0 '{Spiritualitate}.docx
Samael Aun Weor - Stiinta muzicii 1.0 '{Spiritualitate}.docx
Samael Aun Weor - Tarot si Kabala 2.0 '{Spiritualitate}.docx
Samael Aun Weor - Trandafirul ignic 1.0 '{Spiritualitate}.docx
Samael Aun Weor - Tratat de medicina oculta si magie practica 1.0 '{Spiritualitate}.docx
Samael Aun Weor - Tratat ezoteric de teurgie 1.0 '{Spiritualitate}.docx

./Samantha James:
Samantha James - Pariul 1.0 '{Dragoste}.docx

./Samantha Lynne:
Samantha Lynne - O veche iubire V1 0.99 '{Dragoste}.docx
Samantha Lynne - O veche iubire V2 0.99 '{Dragoste}.docx

./Samantha Shannon:
Samantha Shannon - Sezonul oaselor 1.0 '{Supranatural}.docx

./Samantha Young:
Samantha Young - Pe Strada - V1 Pe strada Dublin 1.0 '{Literatura}.docx
Samantha Young - Pe Strada - V2 Pe strada Londra 1.0 '{Literatura}.docx
Samantha Young - Pe Strada - V3 Pe strada Jamaica 1.0 '{Literatura}.docx
Samantha Young - Pe Strada - V4 Pe strada India 1.0 '{Literatura}.docx
Samantha Young - Pe Strada - V5 Ecouri din strada Scotia 1.0 '{Literatura}.docx
Samantha Young - Pe Strada - V6 Clar de luna pe strada Nightingale 1.0 '{Literatura}.docx

./Sam Bourne:
Sam Bourne - A treia femeie 1.0 '{Thriller}.docx
Sam Bourne - Ultimul testament 1.0 '{Suspans}.docx

./Sam Christer:
Sam Christer - Mostenirea Stonehenge 1.0 '{AventuraIstorica}.docx

./Sam Harris:
Sam Harris - Sfarsitul credintei 1.0 '{Filozofie}.docx

./Sam Savage:
Sam Savage - Firmin. Aventurile unei vieti subterane 1.0 '{Literatura}.docx

./Samuel Beckett:
Samuel Beckett - Asteptandu-l pe Godot 0.8 '{Teatru}.docx
Samuel Beckett - Prima iubire 0.6 '{Romance}.docx

./Samuel Bjork:
Samuel Bjork - Bufnita vaneaza intotdeauna noaptea 1.0 '{Diverse}.docx
Samuel Bjork - Calatoresc singura 1.0 '{Politista}.docx

./Samuel Butler:
Samuel Butler - Erewhon 0.99 '{Diverse}.docx
Samuel Butler - Intoarcerea in Erewhon 0.99 '{Diverse}.docx

./Samuel Griffith:
Samuel Griffith - Batalia pentru Guadalcanal 1.0 '{Razboi}.docx

./Samuel R. Delany:
Samuel R. Delany - Intersectia Einstein 1.0 '{SF}.docx
Samuel R. Delany - Nova 1.0 '{SF}.docx

./Sam van Schaik:
Sam van Schaik - O istorie a Tibetului 1.0 '{Spiritualitate}.docx

./Sana Krasikov:
Sana Krasikov - Patriotii 1.0 '{Literatura}.docx

./Sananda:
Sananda - V05 Magia interioara 0.99 '{Spiritualitate}.docx
Sananda - V08 Natura ADN 0.99 '{Spiritualitate}.docx
Sananda - V11 Arhitectii universului V1 0.99 '{Spiritualitate}.docx
Sananda - V12 Arhitectii universului V2 0.99 '{Spiritualitate}.docx
Sananda - V13 Arhitectii universului V3 0.99 '{Spiritualitate}.docx

./San Antonio:
San Antonio - Adevarul facut varza 2.0 '{Politista}.docx
San Antonio - Ai sa joci cum iti cant eu 2.0 '{Politista}.docx
San Antonio - Altminteri ar fi sfarsitul lumii 2.0 '{Politista}.docx
San Antonio - Am incercat, se poate 3.0 '{Politista}.docx
San Antonio - Am onoarea sa va pun la punct 1.0 '{Politista}.docx
San Antonio - Antigel pentru conita 1.0 '{Politista}.docx
San Antonio - Arhipeleagul mitocanilor 2.0 '{Politista}.docx
San Antonio - Asta-i mort si habar n-are 2.0 '{Politista}.docx
San Antonio - Asta-s eu si nu ma schimb 2.0 '{Politista}.docx
San Antonio - Bombe si bomboane 1.0 '{Politista}.docx
San Antonio - Bravo, doctore Beru 2.0 '{Politista}.docx
San Antonio - Bulion pe canapea 1.0 '{Politista}.docx
San Antonio - Cantec de leagan pentru Beru 2.0 '{Politista}.docx
San Antonio - Circulati! N-aveti ce vedea 2.0 '{Politista}.docx
San Antonio - Circul sifiliticilor 2.0 '{Politista}.docx
San Antonio - Clientela pentru morga 0.8 '{Politista}.docx
San Antonio - Colivia fara gratii 0.9 '{Politista}.docx
San Antonio - Da buna ziua la doamna 1.0 '{Politista}.docx
San Antonio - Daca tanti le avea 0.9 '{Politista}.docx
San Antonio - Descaleca si du-te 1.0 '{Politista}.docx
San Antonio - Dezmat la banca indragostitilor 0.9 '{Politista}.docx
San Antonio - Doamnelor, va place tare 1.0 '{Politista}.docx
San Antonio - Draga paseaza-mi microbii tai 1.0 '{Politista}.docx
San Antonio - E periculos sa porti coarne 0.9 '{Politista}.docx
San Antonio - Foc din plin la caraliu 2.0 '{Politista}.docx
San Antonio - Incepe actiunea 1.0 '{Politista}.docx
San Antonio - Infernul verde 1.0 '{Politista}.docx
San Antonio - In meniu, un curcan fript 1.0 '{Politista}.docx
San Antonio - Jos labele 0.9 '{Politista}.docx
San Antonio - Lichidam si ne luam valea 2.0 '{Politista}.docx
San Antonio - Lichidati-l 1.0 '{Politista}.docx
San Antonio - Lupul travestit in bunicuta 0.9 '{Politista}.docx
San Antonio - Mananca iaurt daca mai ai chef 1.0 '{Politista}.docx
San Antonio - Merge, San Antonio 1.0 '{Politista}.docx
San Antonio - Mutre de inmormantare 1.0 '{Politista}.docx
San Antonio - Obsedatul 1.0 '{Politista}.docx
San Antonio - O crunta spargere de nunta 1.0 '{Politista}.docx
San Antonio - Pe socoteala printesei 0.99 '{Politista}.docx
San Antonio - Pieptanand girafa 1.0 '{Politista}.docx
San Antonio - Pune-ti slipul, gondolier 2.0 '{Politista}.docx
San Antonio - Redu presiunea ca plesnesc 1.0 '{Politista}.docx
San Antonio - Sa fim logici 2.0 '{Politista}.docx
San Antonio - Salutare parintele 1.0 '{Politista}.docx
San Antonio - Sarabanda raposatilor 1.0 '{Politista}.docx
San Antonio - Sarutari unde stii tu 1.0 '{Politista}.docx
San Antonio - Se ingroasa gluma 3.0 '{Politista}.docx
San Antonio - Serenada pentru o pipita defuncta 0.9 '{Politista}.docx
San Antonio - Si, signore 0.7 '{Politista}.docx
San Antonio - Sirop pentru viespi 1.0 '{Politista}.docx
San Antonio - Splina in suc propriu 1.0 '{Politista}.docx
San Antonio - Talharia de la unchiul Tom 2.0 '{Politista}.docx
San Antonio - Temperamentali tipii 2.0 '{Politista}.docx
San Antonio - Tombola pungasilor 0.7 '{Politista}.docx
San Antonio - Trage-ma in poza 1.0 '{Politista}.docx
San Antonio - Un criminal perfid 1.0 '{Politista}.docx
San Antonio - Urmariti, prindeti si aduceti Whisky 1.0 '{Politista}.docx
San Antonio - Vino cu lumanarea pregatita 0.9 '{Politista}.docx
San Antonio - Vivat Bertaga! 1.0 '{Politista}.docx
San Antonio - Votati-l pe Berurier! 1.0 '{Politista}.docx

./Sanda & Vlad Stolojan:
Sanda & Vlad Stolojan - Sa nu plecam toti odata 1.0 '{Istorie}.docx

./Sanda Cordos:
Sanda Cordos - Lumea 0.99 '{Diverse}.docx

./Sanda Stefan:
Sanda Stefan - Valeriu Popa - Vindecarea prin gandire 0.8 '{Spiritualitate}.docx

./Sandor Marai:
Sandor Marai - Lumanarile ard pana la capat 1.0 '{Literatura}.docx
Sandor Marai - Mostenirea Eszterei 1.0 '{Literatura}.docx
Sandor Marai - Turneu la Bolzano 1.0 '{Literatura}.docx

./Sandra Brown:
Sandra Brown - 22 Indigo Place 1.5 '{Romance}.docx
Sandra Brown - Ademenirea 1.0 '{Romance}.docx
Sandra Brown - Caderea lui Adam 2.0 '{Romance}.docx
Sandra Brown - Cantec de dragoste 1.5 '{Romance}.docx
Sandra Brown - Casatoria 1.0 '{Romance}.docx
Sandra Brown - Chiar si ingerii cad 2.0 '{Romance}.docx
Sandra Brown - Complotul voluptatii 1.0 '{Romance}.docx
Sandra Brown - Confruntarea 1.0 '{Romance}.docx
Sandra Brown - Copilul nascut joi 2.0 '{Romance}.docx
Sandra Brown - Dantela neagra 1.0 '{Romance}.docx
Sandra Brown - Descoperirea 1.5 '{Romance}.docx
Sandra Brown - Dragoste fierbinte 1.0 '{Romance}.docx
Sandra Brown - Dragoste in catifea albastra 2.0 '{Romance}.docx
Sandra Brown - Dragoste mai presus de ratiune 1.0 '{Romance}.docx
Sandra Brown - Dragoste si datorie 1.0 '{Romance}.docx
Sandra Brown - Dulce manie 1.0 '{Romance}.docx
Sandra Brown - Dupa 10 ani 1.5 '{Romance}.docx
Sandra Brown - Eden Pass 2.0 '{Romance}.docx
Sandra Brown - Fanta C 1.0 '{Romance}.docx
Sandra Brown - Fascinatie 1.0 '{Romance}.docx
Sandra Brown - Fii binevenit 1.0 '{Romance}.docx
Sandra Brown - Focul mocnit din Paradis 1.0 '{Romance}.docx
Sandra Brown - Focuri ascunse 2.0 '{Romance}.docx
Sandra Brown - Fuga de celebritate 0.9 '{Romance}.docx
Sandra Brown - Furtuna in Paradis 1.0 '{Romance}.docx
Sandra Brown - Imaginea din oglinda 2.0 '{Romance}.docx
Sandra Brown - Imbratisare in amurg 1.0 '{Romance}.docx
Sandra Brown - Inima rastignita 1.0 '{Romance}.docx
Sandra Brown - Interviul 1.0 '{Romance}.docx
Sandra Brown - Intotdeauna iubirea 1.0 '{Romance}.docx
Sandra Brown - Labirintul sperantei 1.0 '{Romance}.docx
Sandra Brown - La capatul lumii 0.99 '{Romance}.docx
Sandra Brown - Lacrimi uitate 0.99 '{Romance}.docx
Sandra Brown - Lasa-ma sa te iubesc 1.0 '{Romance}.docx
Sandra Brown - Lunga asteptare 1.0 '{Romance}.docx
Sandra Brown - Martora 1.0 '{Romance}.docx
Sandra Brown - Matase frantuzeasca 0.9 '{Romance}.docx
Sandra Brown - Mostenitoarea 1.0 '{Romance}.docx
Sandra Brown - Neasteptata iubire 1.5 '{Romance}.docx
Sandra Brown - Noapte africana 1.5 '{Romance}.docx
Sandra Brown - Numai tu 1.0 '{Romance}.docx
Sandra Brown - Obligat de onoare 1.0 '{Romance}.docx
Sandra Brown - Ostateca lui Hawk 1.0 '{Romance}.docx
Sandra Brown - Panza de paianjen 1.0 '{Romance}.docx
Sandra Brown - Paravanul de fum 0.9 '{Romance}.docx
Sandra Brown - Regasire 1.0 '{Romance}.docx
Sandra Brown - Sa nu trimiti flori 1.5 '{Romance}.docx
Sandra Brown - Sarada 1.0 '{Romance}.docx
Sandra Brown - Sarutul ispitei 1.0 '{Romance}.docx
Sandra Brown - Sa vorbim despre dragoste 1.0 '{Romance}.docx
Sandra Brown - Secrete bine pazite 1.0 '{Romance}.docx
Sandra Brown - Seductie 1.0 '{Romance}.docx
Sandra Brown - Spune-mi da 1.0 '{Romance}.docx
Sandra Brown - Texas - V1 Texas Lucky 1.0 '{Romance}.docx
Sandra Brown - Texas - V2 Texas Chase 1.0 '{Romance}.docx
Sandra Brown - Texas - V3 Texas Sage 1.0 '{Romance}.docx
Sandra Brown - Traieste clipa 1.0 '{Romance}.docx
Sandra Brown - Tu esti tatal copilului meu 1.0 '{Romance}.docx
Sandra Brown - Ultimul joc 1.0 '{Romance}.docx
Sandra Brown - Umbre din trecut 0.7 '{Romance}.docx

./Sandra Chastain:
Sandra Chastain - Dimineata de dupa 0.7 '{Romance}.docx
Sandra Chastain - O licitatie de burlaci 0.8 '{Romance}.docx

./Sandra Donovan:
Sandra Donovan - Parfumul primejdios al seductiei 1.0 '{Romance}.docx

./Sandra Lewis:
Sandra Lewis - Ochii vulturului 0.9 '{Romance}.docx
Sandra Lewis - Un domn misterios 0.9 '{Dragoste}.docx
Sandra Lewis - Verisoara Caroline 0.99 '{Romance}.docx

./Sandrone Dazieri:
Sandrone Dazieri - Colomba Caselli - V3 Regele de arginti 1.0 '{Literatura}.docx
Sandrone Dazieri - Ingerul 1.0 '{Politista}.docx
Sandrone Dazieri - Ucide-l pe tata 1.0 '{Politista}.docx

./Sandro Petraglia & Stefano Rulli & Andrea Purgatori:
Sandro Petraglia & Stefano Rulli & Andrea Purgatori - Caracatita V5 1.0 '{Politista}.docx

./Sandy Keene:
Sandy Keene - In lumea petrolului 0.99 '{Dragoste}.docx
Sandy Keene - Secrete din trecut 0.9 '{Romance}.docx
Sandy Keene - Teama de necunoscut 0.99 '{Dragoste}.docx

./Santa Montefiore:
Santa Montefiore - Casa de la malul marii 1.1 '{Literatura}.docx

./Sara Craven:
Sara Craven - Zori intunecati 1.0 '{Romance}.docx

./Sara Gruen:
Sara Gruen - Apa pentru elefanti 1.1 '{Literatura}.docx

./Sarah Addison Allen:
Sarah Addison Allen - Gradina fermecata 1.0 '{Literatura}.docx

./Sarah Dunant:
Sarah Dunant - Transgresiuni 0.9 '{Diverse}.docx

./Sarah Goodall & Nicholas Monson:
Sarah Goodall & Nicholas Monson - Jurnalul meu la palat 1.0 '{Politica}.docx

./Sarah J. Maas:
Sarah J. Maas - Regatul Spinilor si al Trandafirilor - V1 Regatul spinilor si al trandafirilor 1.0 '{AventuraIstorica}.docx
Sarah J. Maas - Regatul Spinilor si al Trandafirilor - V2 Regatul cetii si al furiei 1.0 '{AventuraIstorica}.docx
Sarah J. Maas - Regatul Spinilor si al Trandafirilor - V3 Regatul aripilor si al pieirii 1.0 '{AventuraIstorica}.docx
Sarah J. Maas - Tronul de Clestar - V1 Tronul de clestar 2.0 '{AventuraIstorica}.docx
Sarah J. Maas - Tronul de Clestar - V2 Diamantul de la miezul noptii 2.0 '{AventuraIstorica}.docx
Sarah J. Maas - Tronul de Clestar - V3 Mostenitoarea focului 1.0 '{AventuraIstorica}.docx
Sarah J. Maas - Tronul de Clestar - V4 Regina umbrelor 1.0 '{AventuraIstorica}.docx
Sarah J. Maas - Tronul de Clestar - V5 Imperiul furtunilor 0.9 '{AventuraIstorica}.docx
Sarah J. Maas - Tronul de Clestar - V6 Turnul zorilor 0.9 '{AventuraIstorica}.docx

./Sarah Jio:
Sarah Jio - Anotimpul regasirii 1.0 '{Literatura}.docx
Sarah Jio - Destine inlantuite 1.0 '{Literatura}.docx
Sarah Jio - Toate florile Parisului 1.0 '{Literatura}.docx

./Sarah Lark:
Sarah Lark - In Tara Norului Alb - V1 In tara norului alb 1.0 '{Literatura}.docx
Sarah Lark - In Tara Norului Alb - V2 Cantecul maorilor 1.0 '{Literatura}.docx
Sarah Lark - In Tara Norului Alb - V3 Strigatul pasarii Kiwi 1.0 '{Literatura}.docx

./Sarah MacLean:
Sarah MacLean - Sezonul pasiunilor 0.99 '{Literatura}.docx

./Sarah Maclean:
Sarah Maclean - Love by Numbers - V1 Noua reguli de nesocotit 1.0 '{Romance}.docx
Sarah Maclean - Love by Numbers - V2 Zece feluri de a fi adorata 1.0 '{Romance}.docx

./Sara Holland:
Sara Holland - Everless 1.0 '{Supranatural}.docx

./Sarah Orne Jewett:
Sarah Orne Jewett - Tara brazilor inalti 1.0 '{Literatura}.docx

./Sarah Perry:
Sarah Perry - Sarpele din Essex 1.0 '{Literatura}.docx

./Sarah Pinborough:
Sarah Pinborough - Prin ochii ei 1.0 '{Thriller}.docx

./Sara Shepard:
Sara Shepard - Jocul Minciunii - V1 Jocul minciunii 1.0 '{AventuraTineret}.docx
Sara Shepard - Jocul Minciunii - V2 Adevar sau provocare 0.9 '{AventuraTineret}.docx
Sara Shepard - Jocul Minciunii - V3 Doua adevaruri si o minciuna 0.9 '{AventuraTineret}.docx
Sara Shepard - Micutele Mincinoase - V1 Micutele mincinoase 1.0 '{AventuraTineret}.docx
Sara Shepard - Micutele Mincinoase - V2 Impecabil 1.0 '{AventuraTineret}.docx
Sara Shepard - Micutele Mincinoase - V3 Perfect 1.0 '{AventuraTineret}.docx
Sara Shepard - Micutele Mincinoase - V4 Incredibil 1.0 '{AventuraTineret}.docx
Sara Shepard - Micutele Mincinoase si Secretele Lor - V1 Secretul Hannei 1.1 '{AventuraTineret}.docx
Sara Shepard - Micutele Mincinoase si Secretele Lor - V2 Secretul lui Emily 1.1 '{AventuraTineret}.docx
Sara Shepard - Micutele Mincinoase si Secretele Lor - V3 Secretul Ariei 1.1 '{AventuraTineret}.docx
Sara Shepard - Micutele Mincinoase si Secretele Lor - V4 Secretul lui Spencer 1.0 '{AventuraTineret}.docx

./Sarmiza Cretzianu:
Sarmiza Cretzianu - Cronica staicului 1.0 '{Dragoste}.docx

./Sascha Arango:
Sascha Arango - Adevarul si alte minciuni 1.0 '{Literatura}.docx

./Saul Bellow:
Saul Bellow - Darul lui Humboldt 0.7 '{Literatura}.docx
Saul Bellow - Herzog 0.7 '{Literatura}.docx
Saul Bellow - Traieste-ti clipa 0.99 '{Literatura}.docx

./Savatie Bastovoi:
Savatie Bastovoi - Fuga spre campul cu ciori 0.8 '{Diverse}.docx
Savatie Bastovoi - Iepurii 0.99 '{Diverse}.docx

./Saviana Stanescu:
Saviana Stanescu - Apocalipsa gonflabila 0.99 '{Teatru}.docx

./Savu I. Dumitrescu:
Savu I. Dumitrescu - 10 ani alaturi de Marin Preda 1.0 '{ClasicRo}.docx

./Sax Rohmer:
Sax Rohmer - Imparatul Americii 0.7 '{Suspans}.docx

./Saxton Burr:
Saxton Burr - Asaltul turnului 1.0 '{SF}.docx

./Sayaka Murata:
Sayaka Murata - Femeia Minimarket 1.0 '{Literatura}.docx

./Scarlat Demetrescu:
Scarlat Demetrescu - Din tainele vietii si ale universului 2.0 '{Spiritualitate}.docx
Scarlat Demetrescu - Viata dincolo de mormant 2.0 '{Spiritualitate}.docx

./Scott & Cheri Scheer:
Scott & Cheri Scheer - Caramizile casniciei 0.99 '{Psihologie}.docx

./Scott Bergstrom:
Scott Bergstrom - The Cruelty - V1 Cruzimea 1.0 '{Politista}.docx

./Scott Lynch:
Scott Lynch - Ticalosul Gentilom - V1 Minciunile lui Locke Lamora 1.0 '{AventuraIstorica}.docx
Scott Lynch - Ticalosul Gentilom - V2 Piratii marilor rosii 1.0 '{AventuraIstorica}.docx

./Scott Mariani:
Scott Mariani - Benedict Hope - V1 Manuscrisul lui Fulcanelli 2.0 '{Thriller}.docx
Scott Mariani - Benedict Hope - V2 Conspiratia Mozart 2.0 '{Thriller}.docx

./Scott Mcbain:
Scott Mcbain - Argintii lui Iuda 1.0 '{Thriller}.docx

./Scott O. Lilienfeld:
Scott O. Lilienfeld - 50 de mari mituri ale psihologiei populare 1.0 '{Psihologie}.docx

./Scott Smith:
Scott Smith - Ruinele 1.0 '{Diverse}.docx
Scott Smith - Un plan simplu 1.0 '{Diverse}.docx

./Scott Turow:
Scott Turow - Eroi obisnuiti 1.0 '{Thriller}.docx
Scott Turow - Prejudicii morale 0.8 '{Thriller}.docx
Scott Turow - Vina marturisita 0.99 '{Thriller}.docx
Scott Turow - Vinovatie dovedita 0.99 '{Thriller}.docx

./Scratty:
Scratty - Poarta temporala din Romania 0.2 '{SF}.docx

./Scufita Rosie:
Scufita Rosie - Un alt mod de a privi lucrurile 0.8 '{Erotic}.docx

./Seanan McGuire:
Seanan McGuire - Copii in Deriva - V1 Orice inima e o poarta 1.0 '{AventuraTineret}.docx

./Sean Mcmullen:
Sean Mcmullen - Glasul de otel 0.99 '{Diverse}.docx

./Sean Wilson:
Sean Wilson - V1 Misiune pentru mercenari 1.0 '{ActiuneComando}.docx
Sean Wilson - V2 Ruleta Ruseasca 1.0 '{ActiuneComando}.docx

./Sebastian A. Corn:
Sebastian A. Corn - 2484 Quirinal Ave 0.99 '{SF}.docx
Sebastian A. Corn - A saptea zi a vanatorului 0.9 '{SF}.docx
Sebastian A. Corn - Cartea brundurilor 2.0 '{SF}.docx
Sebastian A. Corn - Cartea lui Anton Canta 0.99 '{SF}.docx
Sebastian A. Corn - Scrisoare 0.99 '{SF}.docx
Sebastian A. Corn - Ultima zi a vanatorului 0.99 '{SF}.docx

./Sebastian de Castell:
Sebastian de Castell - Duelul Vrajilor - V1 Duelul vrajilor 1.0 '{Supranatural}.docx

./Sebastien Japrisot:
Sebastien Japrisot - Compartimentul ucigasilor 1.0 '{Politista}.docx
Sebastien Japrisot - Doamna din masina 1.0 '{Politista}.docx

./Seiko Tsukioka:
Seiko Tsukioka - Creatie sau evolutie 0.99 '{MistersiStiinta}.docx

./Selina Brent & Tom Coleman:
Selina Brent & Tom Coleman - Zbor blestemat 1.0 '{ActiuneComando}.docx

./Selma Lagerlof:
Selma Lagerlof - Legende despre Iisus 0.3 '{Tineret}.docx
Selma Lagerlof - Minunata calatorie a lui Nils Holgersson prin Suedia 1.0 '{Tineret}.docx

./Serafim Rose:
Serafim Rose - Cauzele revolutiei din epoca moderna. Nihilismul 0.9 '{Religie}.docx
Serafim Rose - Ortodoxia si religia viitorului 1.0 '{Religie}.docx
Serafim Rose - Semnele vremurilor 0.9 '{Religie}.docx
Serafim Rose - Sufletul dupa moarte 1.0 '{Religie}.docx
Serafim Rose - Teologul rugator 0.9 '{Religie}.docx

./Serban Constantinescu:
Serban Constantinescu - Departe de izvor 0.9 '{Religie}.docx
Serban Constantinescu - Nazbatii teologice 0.9 '{Religie}.docx

./Serge Bernstein:
Serge Bernstein - Istoria Europei 2.0 '{Istorie}.docx

./Serge Brussolo:
Serge Brussolo - Anamorfoza sau legaturile de sange 2.0 '{Horror}.docx
Serge Brussolo - Bulevardul Banchizelor 3.0 '{Horror}.docx
Serge Brussolo - Ca o oglinda moarta 2.0 '{Horror}.docx
Serge Brussolo - Carnavalul de fier 4.0 '{Horror}.docx
Serge Brussolo - Coloana a sasea 2.0 '{Horror}.docx
Serge Brussolo - Copiii lui Proteu 2.0 '{Horror}.docx
Serge Brussolo - Cosmar de inchiriat 3.0 '{Horror}.docx
Serge Brussolo - Destroy 3.0 '{Horror}.docx
Serge Brussolo - Doctorul Schelet 3.0 '{Horror}.docx
Serge Brussolo - Febra 3.0 '{Horror}.docx
Serge Brussolo - Haita 3.0 '{Horror}.docx
Serge Brussolo - Iceberg 3.0 '{Horror}.docx
Serge Brussolo - Interviu in exclusivitate cu Serge Brussolo 2.0 '{Horror}.docx
Serge Brussolo - Ira Melanox 3.0 '{Horror}.docx
Serge Brussolo - Jurnal neterminat. Memoriile lui Vivo 2.0 '{Horror}.docx
Serge Brussolo - Kamikaze spatiali 4.0 '{Horror}.docx
Serge Brussolo - Krucifix 4.0 '{Horror}.docx
Serge Brussolo - Labirintul 3.0 '{Horror}.docx
Serge Brussolo - Lacate carnivore 4.0 '{Horror}.docx
Serge Brussolo - Mana rece 1.0 '{Horror}.docx
Serge Brussolo - Mancatorii de ziduri 3.0 '{Horror}.docx
Serge Brussolo - Memorial in vivo. Jurnal neterminat 2.0 '{Horror}.docx
Serge Brussolo - Moartea cu melon 4.0 '{Horror}.docx
Serge Brussolo - Noaptea lupului 1.0 '{Horror}.docx
Serge Brussolo - Off 2.0 '{Horror}.docx
Serge Brussolo - Peggy Sue si Fantomele - V1 Ziua cainelui albastru 1.0 '{Horror}.docx
Serge Brussolo - Peggy Sue si Fantomele - V2 Somnul demonului 1.0 '{Horror}.docx
Serge Brussolo - Pelerinii intunericului 1.0 '{Horror}.docx
Serge Brussolo - Soare de sulf 2.0 '{Horror}.docx
Serge Brussolo - Un Ianus al imaginarului 1.0 '{Horror}.docx
Serge Brussolo - V1 Somnul sangelui 4.0 '{Horror}.docx
Serge Brussolo - V2 Trasee si itinerarii ale uitarii 4.0 '{Horror}.docx
Serge Brussolo - V3 Vizita cu ghid 2.0 '{Horror}.docx
Serge Brussolo - V4 Pe cat de greu e vantul 2.0 '{Horror}.docx

./Serge Deville:
Serge Deville - O grenada pentru Heydrich 1.0 '{ActiuneComando}.docx

./Serge Monaste:
Serge Monaste - Protocoalele de la Toronto 0.9 '{Jurnalism}.docx

./Serge Moscovici:
Serge Moscovici - Cronica anilor risipiti 0.7 '{Biografie}.docx

./Serghei Borodin:
Serghei Borodin - Stelele Samarkandului - V1 Timur cel schiop 0.9 '{AventuraIstorica}.docx
Serghei Borodin - Stelele Samarkandului - V2 Parjolul razboiului 1.0 '{AventuraIstorica}.docx

./Serghei Kurdakov:
Serghei Kurdakov - Iarta-ma Natasa 0.99 '{Aventura}.docx

./Serghei Nikolaevici Lazarev:
Serghei Nikolaevici Lazarev - Cauzele imbolnavirilor dupa Lazarev 0.5 '{Spiritualitate}.docx
Serghei Nikolaevici Lazarev - Un renumit bioenergetician rus a descoperit cauzele bolilor 0.9 '{Spiritualitate}.docx
Serghei Nikolaevici Lazarev - V1 Sistemul autoreglarii campurilor 1.0 '{Spiritualitate}.docx
Serghei Nikolaevici Lazarev - V2 Karma pura 0.8 '{Spiritualitate}.docx
Serghei Nikolaevici Lazarev - V3 Iubirea 0.99 '{Spiritualitate}.docx
Serghei Nikolaevici Lazarev - V4 Privire in viitor 0.99 '{Spiritualitate}.docx
Serghei Nikolaevici Lazarev - V5 Raspunsuri la intrebari 0.7 '{Spiritualitate}.docx
Serghei Nikolaevici Lazarev - V6 Trepte catre divinitate 0.7 '{Spiritualitate}.docx
Serghei Nikolaevici Lazarev - V7 Transcenderea fericirii 0.6 '{Spiritualitate}.docx
Serghei Nikolaevici Lazarev - V8 Dialoguri 0.6 '{Spiritualitate}.docx
Serghei Nikolaevici Lazarev - V9 Indrumar de supravietuire 0.7 '{Spiritualitate}.docx
Serghei Nikolaevici Lazarev - V10 Principii de vindecare 0.9 '{Spiritualitate}.docx

./Sergio Pitol:
Sergio Pitol - Imblanzirea divinei egrete 1.0 '{Literatura}.docx

./Sergiu Dan:
Sergiu Dan - Surorile Veniamin 1.0 '{Dragoste}.docx

./Sergiu Farcasan:
Sergiu Farcasan - Atacul cesiumistilor 1.2 '{SF}.docx
Sergiu Farcasan - Manualul bunului carpaci 1.0 '{Umor}.docx
Sergiu Farcasan - Masina de rupt prieteniile 1.0 '{SF}.docx
Sergiu Farcasan - Micul televizor 0.6 '{Umor}.docx
Sergiu Farcasan - O iubire din anul 41042 1.0 '{SF}.docx
Sergiu Farcasan - Secretul inginerului Musat 1.0 '{SF}.docx
Sergiu Farcasan - Va cauta un taur 1.0 '{SF}.docx

./Sergiu Motorgianu:
Sergiu Motorgianu - Am intalnit un inger 1.0 '{Literatura}.docx
Sergiu Motorgianu - Mai mult decat un semn 0.9 '{Literatura}.docx

./Sergiu Somesan:
Sergiu Somesan - Adio Efida 0.99 '{SF}.docx
Sergiu Somesan - Apa vie, apa moarta 0.99 '{SF}.docx
Sergiu Somesan - Apocalipsa dupa Ceausescu 5.0 '{SF}.docx
Sergiu Somesan - Bla bla bla domnule general 0.99 '{SF}.docx
Sergiu Somesan - Carte de magie 5.0 '{SF}.docx
Sergiu Somesan - Ceara pierduta 0.99 '{SF}.docx
Sergiu Somesan - Cel din urma Unicorn 5.0 '{SF}.docx
Sergiu Somesan - Cele noua miliarde de idei 0.99 '{SF}.docx
Sergiu Somesan - Dilema 0.99 '{SF}.docx
Sergiu Somesan - Fata de pe malul marii 0.99 '{SF}.docx
Sergiu Somesan - Fauritorul de vise 5.0 '{SF}.docx
Sergiu Somesan - Inchide bine usa 0.99 '{SF}.docx
Sergiu Somesan - Intalnire cu dragonul 1.0 '{SF}.docx
Sergiu Somesan - Intalnire cu Isabel 0.99 '{SF}.docx
Sergiu Somesan - Intalnire cu o zeita 0.99 '{SF}.docx
Sergiu Somesan - Iubitul meu din alta lume 0.99 '{SF}.docx
Sergiu Somesan - Legenda fara sfarsit 0.99 '{SF}.docx
Sergiu Somesan - Lucrare de diploma 0.99 '{SF}.docx
Sergiu Somesan - Merele Evei 0.99 '{SF}.docx
Sergiu Somesan - Narcisa 0.99 '{SF}.docx
Sergiu Somesan - Nascocirea 0.99 '{SF}.docx
Sergiu Somesan - Numarul fiarei 0.99 '{SF}.docx
Sergiu Somesan - O clipa de nemurire 0.99 '{SF}.docx
Sergiu Somesan - O ipoteza aproape absurda 0.99 '{SF}.docx
Sergiu Somesan - O racheta cu numele meu 0.99 '{SF}.docx
Sergiu Somesan - Oul 0.99 '{SF}.docx
Sergiu Somesan - Oviraptorul 0.99 '{SF}.docx
Sergiu Somesan - Panseluta 0.99 '{SF}.docx
Sergiu Somesan - Pescarusul 0.99 '{SF}.docx
Sergiu Somesan - Poarta transcedentala 0.99 '{SF}.docx
Sergiu Somesan - Poiana ingerilor 0.99 '{SF}.docx
Sergiu Somesan - Poveste 0.99 '{SF}.docx
Sergiu Somesan - Povestiri fantastice si S.F. 1.0 '{SF}.docx
Sergiu Somesan - Rafael 1.0 '{SF}.docx
Sergiu Somesan - Ritual de trecere 0.99 '{SF}.docx
Sergiu Somesan - Rozi 0.99 '{SF}.docx
Sergiu Somesan - Sa n-o saruti pe Isabel 5.0 '{SF}.docx
Sergiu Somesan - Sansa noastra s-a numit Adam 0.99 '{SF}.docx
Sergiu Somesan - Taietorul de lemne 0.99 '{SF}.docx
Sergiu Somesan - Timidul 0.99 '{SF}.docx
Sergiu Somesan - Tramvaiul 0.99 '{SF}.docx
Sergiu Somesan - Ultima zi 0.99 '{SF}.docx
Sergiu Somesan - Ultimul visator 0.99 '{SF}.docx
Sergiu Somesan - Vanzatorul de poduri 0.99 '{SF}.docx
Sergiu Somesan - Viorica 0.99 '{SF}.docx
Sergiu Somesan - Vis 0.99 '{SF}.docx
Sergiu Somesan - Vizita de lucru 0.99 '{SF}.docx

./Seth Dickinson:
Seth Dickinson - Tradatoarea Baru Cormoran 1.0 '{AventuraIstorica}.docx

./Seth Godin:
Seth Godin - Toti suntem ciudati 1.0 '{DezvoltarePersonala}.docx

./Sever Gansovski:
Sever Gansovski - Cristalul 0.99 '{SF}.docx
Sever Gansovski - Operatia 0.99 '{SF}.docx

./Sever Noran:
Sever Noran - Comoara care poate ucide 2.0 '{Politista}.docx
Sever Noran - Formula magica V1 1.0 '{ClubulTemerarilor}.docx
Sever Noran - Formula magica V2 1.0 '{ClubulTemerarilor}.docx
Sever Noran - Greseala fatala 1.0 '{Politista}.docx
Sever Noran - Secretul Globului de Argint V1 1.0 '{ClubulTemerarilor}.docx
Sever Noran - Secretul Globului de Argint V2 1.0 '{ClubulTemerarilor}.docx
Sever Noran - Sfarsitul Marelui Preot 1.0 '{Politista}.docx
Sever Noran - Ultimul viraj 0.99 '{Politista}.docx

./Sfantul Nectarie:
Sfantul Nectarie - Minuni in Romania 1.0 '{Religie}.docx

./Shane Paddington:
Shane Paddington - Adevarata libertate 0.9 '{Romance}.docx

./Shannon Anderson:
Shannon Anderson - Destinul lui Kathleen 1.0 '{Romance}.docx

./Shari Lapena:
Shari Lapena - Cuplul din vecini 1.0 '{Thriller}.docx
Shari Lapena - Strainul de acasa 1.0 '{Thriller}.docx

./Sharon Di Meglio:
Sharon Di Meglio - Farmecul lui Mackenzie 0.99 '{Dragoste}.docx

./Sharon Huss Roat:
Sharon Huss Roat - Cum sa (nu) ma vezi 1.0 '{Literatura}.docx

./Sharon Phillips:
Sharon Phillips - Abisul 0.99 '{Dragoste}.docx

./Sharon Sala:
Sharon Sala - Mostenitorul indragostit 0.9 '{Dragoste}.docx

./Sharon Sayler:
Sharon Sayler - Limbajul corpului tau 0.9 '{DezvoltarePersonala}.docx

./Shauna Michaels:
Shauna Michaels - Ratacire pe taramul tristetii 0.9 '{Romance}.docx

./Shaun Clarke:
Shaun Clarke - Asalt final 1.0 '{ActiuneComando}.docx
Shaun Clarke - Operatiunea Scud 1.0 '{ActiuneComando}.docx
Shaun Clarke - Panica in Malvine 1.0 '{ActiuneComando}.docx

./Sheila Holland:
Sheila Holland - Devotamentul unei infirmiere 0.99 '{Dragoste}.docx
Sheila Holland - Inimi zbuciumate 0.9 '{Romance}.docx
Sheila Holland - Viata la tara 0.99 '{Dragoste}.docx

./Sheila Ross:
Sheila Ross - Apartament de inchiriat 0.99 '{Dragoste}.docx
Sheila Ross - Inca o data 0.99 '{Romance}.docx

./Sherry Lewis:
Sherry Lewis - Copilarie furata 0.7 '{Dragoste}.docx

./Shinichi Hoshi:
Shinichi Hoshi - Rugamintea prietenului 0.9 '{Diverse}.docx
Shinichi Hoshi - Viata blagoslovita 0.7 '{Diverse}.docx
Shinichi Hoshi - Viata trecuta 0.7 '{Diverse}.docx

./Shirley Conran:
Shirley Conran - Lace 1.0 '{Literatura}.docx

./Shirley Jackson:
Shirley Jackson - Casa bantuita 1.0 '{Horror}.docx

./Shirley Warren:
Shirley Warren - Dincolo de stele 0.8 '{Dragoste}.docx

./Shri Mataji Nirmala Devi:
Shri Mataji Nirmala Devi - Energia Kundalini 0.8 '{Spiritualitate}.docx
Shri Mataji Nirmala Devi - O descoperire fara precedent 0.9 '{Spiritualitate}.docx

./Shusaku Endo:
Shusaku Endo - Samuraiul 0.8 '{Diverse}.docx

./Sideny Sheldon:
Sideny Sheldon - Strainul din oglinda 1.0 '{Romance}.docx

./Sidney Horley:
Sidney Horley - Vulturul negru 1.0 '{Aventura}.docx

./Sidney Sheldon:
Sidney Sheldon - Amintiri de la miezul noptii 2.0 '{Romance}.docx
Sidney Sheldon - Casino 1.0 '{Romance}.docx
Sidney Sheldon - Chipul fara masca 2.0 '{Romance}.docx
Sidney Sheldon - Conspiratia 1.0 '{Romance}.docx
Sidney Sheldon - De cealalta parte a miezului noptii 1.0 '{Romance}.docx
Sidney Sheldon - Demonul trecutului 2.0 '{Romance}.docx
Sidney Sheldon - Dimineata, la pranz si seara 1.0 '{Romance}.docx
Sidney Sheldon - Eliminare definitiva 1.0 '{Romance}.docx
Sidney Sheldon - Flori de cires si Coca Cola 1.0 '{Romance}.docx
Sidney Sheldon - Furia ingerilor 1.0 '{Romance}.docx
Sidney Sheldon - In cautarea adevarului 1.0 '{Romance}.docx
Sidney Sheldon - Ingeri cu suflete murdare 0.7 '{Romance}.docx
Sidney Sheldon - Inger si demon 1.0 '{Romance}.docx
Sidney Sheldon - Inrobiti de iubire 2.0 '{Romance}.docx
Sidney Sheldon - Legaturi de sange 1.1 '{Romance}.docx
Sidney Sheldon - Morile de vant ale zeilor 1.0 '{Romance}.docx
Sidney Sheldon - Mostenire blestemata 1.0 '{Romance}.docx
Sidney Sheldon - Nimic nu dureaza o vesnicie 1.0 '{Romance}.docx
Sidney Sheldon - Nisipurile timpului 1.0 '{Romance}.docx
Sidney Sheldon - Operatiunea Delta 1.0 '{Romance}.docx
Sidney Sheldon - Prins in capcana 1.0 '{Romance}.docx
Sidney Sheldon - Printesa de gheata 2.0 '{Romance}.docx
Sidney Sheldon - Razbunare la Seattle 1.0 '{Romance}.docx
Sidney Sheldon - Si daca maine va veni 1.0 '{Romance}.docx
Sidney Sheldon - Sophie 1.0 '{Romance}.docx
Sidney Sheldon - Stapana pe situatie 1.0 '{Romance}.docx
Sidney Sheldon - Stelele stralucesc aproape 1.0 '{Romance}.docx
Sidney Sheldon - Strainul din oglinda 1.0 '{Romance}.docx
Sidney Sheldon - Strazi de foc 1.0 '{Romance}.docx
Sidney Sheldon - Sub semnul dolarului 2.0 '{Romance}.docx
Sidney Sheldon - Suspans 1.0 '{Romance}.docx
Sidney Sheldon - Viata dubla 0.7 '{Romance}.docx
Sidney Sheldon - Virus 2.0 '{Romance}.docx

./Siegfried Lenz:
Siegfried Lenz - Ora de germana 1.0 '{Literatura}.docx
Siegfried Lenz - Un moment de reculegere 1.0 '{Literatura}.docx

./Siegmar Erkner:
Siegmar Erkner - Inaintea plecarii 1.0 '{Diverse}.docx

./Sigmund Freud:
Sigmund Freud - Interpretarea viselor 0.9 '{Filozofie}.docx

./Siluan Athonitul:
Siluan Athonitul - Intre iadul deznadejdii si iadul smereniei 0.7 '{Religie}.docx

./Silvana de Mari:
Silvana de Mari - Ultimul elf 0.9 '{Tineret}.docx

./Silvestru Augustin Prundus & Clemente Plaianu:
Silvestru Augustin Prundus & Clemente Plaianu - Catolicism si ortodoxie 0.9 '{Istorie}.docx

./Silvia Bitere:
Silvia Bitere - Fantastica realitate 0.8 '{Diverse}.docx

./Silvina Ocampo:
Silvina Ocampo - Fluturele 0.9 '{SF}.docx

./Silviu Genescu:
Silviu Genescu - Ghost Story 0.99 '{SF}.docx
Silviu Genescu - Privind spre Titanic 0.9 '{SF}.docx
Silviu Genescu - Viitorul aproape de usa 0.9 '{SF}.docx

./Silvy Sanders:
Silvy Sanders - Copila apelor 0.99 '{Dragoste}.docx
Silvy Sanders - Tacere grea 0.99 '{Dragoste}.docx

./Simion Catau:
Simion Catau - Epigrame si catrene 1.0 '{Versuri}.docx

./Simona Cratel:
Simona Cratel - Rasul Evei 0.99 '{Diverse}.docx

./Simona Modreanu:
Simona Modreanu - Cioran 1.0 '{ClasicRo}.docx

./Simone de Beauvoir:
Simone de Beauvoir - Al doilea sex 0.9 '{Psihologie}.docx
Simone de Beauvoir - Femeia sfasiata 0.8 '{Psihologie}.docx

./Simone Elkeles:
Simone Elkeles - Chimie Perfecta - V1 Chimie perfecta 1.0 '{AventuraTineret}.docx
Simone Elkeles - Chimie Perfecta - V2 Legile atractiei 1.0 '{AventuraTineret}.docx
Simone Elkeles - Chimie Perfecta - V3 Reactie in lant 0.99 '{AventuraTineret}.docx

./Simone Gyorfi:
Simone Gyorfi - O poveste pentru fiecare 1.0 '{BasmesiPovesti}.docx

./Simon Green:
Simon Green - Zona Intunecata - V1 Zona intunecata 2.0 '{SF}.docx
Simon Green - Zona Intunecata - V2 Trimisii luminii si ai intunericului 2.0 '{SF}.docx
Simon Green - Zona Intunecata - V3 Tanguirea privighetorii 2.0 '{SF}.docx

./Simon Scarrow:
Simon Scarrow - Centurionul 1.0 '{AventuraIstorica}.docx
Simon Scarrow - Gladiatorul 1.0 '{AventuraIstorica}.docx
Simon Scarrow - Pretorianul 1.0 '{AventuraIstorica}.docx

./Simon Sebag Montefiore:
Simon Sebag Montefiore - Ierusalim 1.0 '{Biografie}.docx
Simon Sebag Montefiore - Moscova - V1 Sasenka 1.0 '{Literatura}.docx
Simon Sebag Montefiore - Moscova - V2 Cerul rosu al amiezii 1.0 '{Razboi}.docx
Simon Sebag Montefiore - Moscova - V3 Intr-o noapte de iarna 1.0 '{Literatura}.docx

./Simon Sinek:
Simon Sinek - Intreaba-te de ce 1.0 '{DezvoltarePersonala}.docx

./Simon Spurrier:
Simon Spurrier - Supravietuitorul 3.0 '{SF}.docx

./Simon Toyne:
Simon Toyne - Trilogia Sancti - V1 Sanctus 2.0 '{Thriller}.docx
Simon Toyne - Trilogia Sancti - V2 Cheia 2.0 '{Thriller}.docx
Simon Toyne - Trilogia Sancti - V3 Turnul 2.0 '{Thriller}.docx

./Simon Urban:
Simon Urban - Planul D 1.0 '{Thriller}.docx

./Sinclair Lewis:
Sinclair Lewis - Dodsworth 1.0 '{Literatura}.docx

./Siobhan Dowd:
Siobhan Dowd - Misterul de la London Eye 1.0 '{Thriller}.docx

./Sioux Berger:
Sioux Berger - Zen in 10 minute 0.7 '{DezvoltarePersonala}.docx

./Siri Hustvedt:
Siri Hustvedt - O vara fara barbati 1.0 '{Literatura}.docx

./Slavomir Nastasijevic:
Slavomir Nastasijevic - Hannibal Ante Portas 1.0 '{AventuraIstorica}.docx

./Slavomir Rawicz:
Slavomir Rawicz - Intoarcerea acasa 1.0 '{Literatura}.docx

./Smaranda Vornicu:
Smaranda Vornicu - Se plang de mine ingerii 0.99 '{Versuri}.docx

./Snowdon King:
Snowdon King - Biblos 0.99 '{SF}.docx
Snowdon King - Iesiti afara din casa 0.8 '{SF}.docx
Snowdon King - Jocul lui Lucifer 0.99 '{SF}.docx
Snowdon King - Penitenciarul 0.99 '{SF}.docx
Snowdon King - Perechea de rezerva 0.99 '{SF}.docx
Snowdon King - Picatura 0.7 '{SF}.docx
Snowdon King - The human game 0.99 '{SF}.docx
Snowdon King - Uezen 0.7 '{SF}.docx
Snowdon King - Ultimele 6 minute 0.99 '{SF}.docx

./Sofia Lundberg:
Sofia Lundberg - Caietul cu nume pierdute 1.0 '{Dragoste}.docx
Sofia Lundberg - Un semn de intrebare este o jumatate de inima 1.0 '{Dragoste}.docx

./Sofian Boghiu:
Sofian Boghiu - Smerenia si dragostea 0.7 '{Religie}.docx

./Sofia Samatar:
Sofia Samatar - Un strain in Olondria 1.0 '{Fantasy}.docx

./Solmaz Kamuran:
Solmaz Kamuran - Kiraze 1.0 '{Literatura}.docx

./Solomon Northup:
Solomon Northup - 12 ani de sclavie 1.0 '{Literatura}.docx

./Somaiya Daud:
Somaiya Daud - Miraj 1.0 '{Literatura}.docx

./Sondra Stanford:
Sondra Stanford - Meandrele sufletului 0.99 '{Romance}.docx

./Sonia Deane:
Sonia Deane - Prada geloziei 0.99 '{Romance}.docx

./Sonia Palty:
Sonia Palty - Evrei, treceti Nistrul 1.0 '{Razboi}.docx

./Sonja Szimon:
Sonja Szimon - S-a intamplat la Turda 0.9 '{Diverse}.docx

./Sophia Nardin:
Sophia Nardin - Ferma lui Falcon 0.99 '{Dragoste}.docx
Sophia Nardin - Nu ma grabi 0.99 '{Dragoste}.docx
Sophia Nardin - Razbunare neintemeiata 0.9 '{Dragoste}.docx

./Sophie Audouin Mamikonian:
Sophie Audouin Mamikonian - Tara Duncan - V6 In capcana lui Magister 0.9 '{Tineret}.docx

./Sophie de Villenoisy:
Sophie de Villenoisy - Sinucidere fericita si la multi ani! 1.0 '{Diverse}.docx

./Sophie Hannah:
Sophie Hannah - Crime cu monograma 1.0 '{Politista}.docx
Sophie Hannah - Greseala fatala 1.0 '{Thriller}.docx
Sophie Hannah - O papusa sau alta 1.0 '{Thriller}.docx

./Soren Kierkegaard:
Soren Kierkegaard - Jurnalul seducatorului 0.9 '{Jurnal}.docx

./Sorina Cassvan:
Sorina Cassvan - Intre pana si spada 1.0 '{AventuraIstorica}.docx

./Sorin Cerin:
Sorin Cerin - Invata sa mori 0.99 '{Filozofie}.docx

./Sorin Coada:
Sorin Coada - Carausul 0.99 '{ProzaScurta}.docx
Sorin Coada - In noapte 0.99 '{ProzaScurta}.docx
Sorin Coada - In tren 0.99 '{ProzaScurta}.docx
Sorin Coada - La copca 0.99 '{ProzaScurta}.docx
Sorin Coada - Limoniu 0.99 '{ProzaScurta}.docx
Sorin Coada - Sarpele midgardului 0.99 '{ProzaScurta}.docx

./Sorin Holban:
Sorin Holban - Sa nu astepti minuni de la luna septembrie 1.0 '{Dragoste}.docx

./Sorin Olariu:
Sorin Olariu - Cartea-i mica, dar furnica 0.9 '{Epigrame}.docx
Sorin Olariu - Epigrame - Mica antologie 1.0 '{Epigrame}.docx
Sorin Olariu - Uica Nielu sa intoarse - Poezie in grai banatean 0.99 '{Versuri}.docx
Sorin Olariu - Un zambet de peste ocean 1.0 '{Epigrame}.docx

./Sorin Stefanescu:
Sorin Stefanescu - Zee 1.0 '{SF}.docx

./Spencer Johnson:
Spencer Johnson - Cine mi-a luat cascavalul 1.0 '{DezvoltarePersonala}.docx

./Spider Robinson:
Spider Robinson - Melancolia elefantilor 0.99 '{ProzaScurta}.docx
Spider Robinson - Urechea de tinichea 0.99 '{SF}.docx

./Stacey Kade:
Stacey Kade - Naluca si Naucul - V1 Naluca si naucul 1.1 '{AventuraTineret}.docx

./Stanislav Lacomchin:
Stanislav Lacomchin - Lala 0.99 '{SF}.docx

./Stanislaw Lem:
Stanislaw Lem - Catarul 0.99 '{SF}.docx
Stanislaw Lem - Ciberiada 1.0 '{SF}.docx
Stanislaw Lem - Eden 0.99 '{SF}.docx
Stanislaw Lem - Edificiul nebuniei absolute 1.0 '{SF}.docx
Stanislaw Lem - Golem 14 0.9 '{SF}.docx
Stanislaw Lem - Intoarcerea din stele 0.9 '{SF}.docx
Stanislaw Lem - Solaris 6.0 '{SF}.docx

./Stanley G. Weinbaum:
Stanley G. Weinbaum - Idealul 0.99 '{SF}.docx
Stanley G. Weinbaum - Insula Proteus 0.99 '{SF}.docx
Stanley G. Weinbaum - Luna nebuna 0.99 '{SF}.docx
Stanley G. Weinbaum - Valea viselor 0.99 '{SF}.docx

./Stan Nicholls:
Stan Nicholls - Orcii - V1 Paznicul fulgerului 1.0 '{Horror}.docx
Stan Nicholls - Orcii - V2 Legiunea tunetului 1.0 '{Horror}.docx
Stan Nicholls - Orcii - V3 Razboinicii furtunii 1.0 '{Horror}.docx

./Starcraft:
Starcraft - V1 Jeff Grubb - Cruciada lui Liberty 0.8 '{SF}.docx
Starcraft - V2 Gabriel Mesta - Umbra Xel Naga 0.8 '{SF}.docx
Starcraft - V3 Tracy Hickman - Viteza intunericului 0.8 '{SF}.docx

./Staretul Tadei de la Vitovnita:
Staretul Tadei de la Vitovnita - Cum iti sunt gandurile astfel iti este si viata 0.99 '{Religie}.docx

./Star Trek:
Star Trek - Alan Dean Foster - Jurnalul 1 1.0 '{SF}.docx
Star Trek - Alan Dean Foster - Jurnalul 2 2.0 '{SF}.docx
Star Trek - Alan Dean Foster - Jurnalul 3 1.0 '{SF}.docx
Star Trek - Alan Dean Foster - Jurnalul 4 1.0 '{SF}.docx
Star Trek - Alan Dean Foster - Jurnalul 5 1.0 '{SF}.docx
Star Trek - Alan Dean Foster - Jurnalul 6 1.0 '{SF}.docx

./Star Trek Tng:
Star Trek Tng - V1 Diane Carey - Vasul fantoma 1.0 '{SF}.docx
Star Trek Tng - V2 Dewesse Gene - Gardienii pacii 1.0 '{SF}.docx
Star Trek Tng - V3 Carmen Carter - Copiii de pe Hamlin 1.0 '{SF}.docx
Star Trek Tng - V4 Jean Lorrah - Supravietuitorii 1.0 '{SF}.docx

./Star Wars:
Star Wars - Alan Dean Foster - Trezirea Fortei 1.0 '{SF}.docx
Star Wars - Dave Wolverton - Ascensiunea fortei 2.0 '{SF}.docx
Star Wars - James Luceno - Darth Maul - Sabotor 1.0 '{SF}.docx
Star Wars - James Luceno - Darth Plagueis 1.0 '{SF}.docx
Star Wars - Jason Fry  - Episodul 8 - Ultimul Jedi 1.0 '{SF}.docx
Star Wars - Jude Watson - Avertismentele Flotei de Asalt 1.0 '{SF}.docx
Star Wars - Matthew Stover - Echipament 1.0 '{SF}.docx
Star Wars - Noul ordin Jedi - Karen Traviss - Boba Fett - Un barbat practic 1.0 '{SF}.docx
Star Wars - Timothy Zahn - Duel 1.0 '{SF}.docx
Star Wars - V00 Despre fenomenul Star Wars 1.0 '{SF}.docx
Star Wars - V01 James Luceno - Mantia inselaciunii 2.2 '{SF}.docx
Star Wars - V02 Michael Reaves - Darth Maul vanatorul din umbra 1.1 '{SF}.docx
Star Wars - V03 Jude Watson - Fantomele Lorzilor Sith 2.2 '{SF}.docx
Star Wars - V03 Terry Brooks - Amenintarea fantomei 2.0 '{SF}.docx
Star Wars - V04 Greg Bear - Planeta adormita 1.1 '{SF}.docx
Star Wars - V05 Timothy Zahn - Zbor in necunoscut 2.0 '{SF}.docx
Star Wars - V06 Allan Dean Foster - Apropierea furtunii 2.0 '{SF}.docx
Star Wars - V07 R. A. Salvadore - Atacul clonelor 2.0 '{SF}.docx
Star Wars - V08 Matthew Stover - La rascruce 1.0 '{SF}.docx
Star Wars - V09 Steven Barnes - Complotul de pe Cestus 1.0 '{SF}.docx
Star Wars - V10 Sean Steward - Yoda - Randes vous intunecat 1.0 '{SF}.docx
Star Wars - V11 James Luceno - Labirintul raului 1.0 '{SF}.docx
Star Wars - V12 Matthew Stover - Razbunarea Sith 1.0 '{SF}.docx
Star Wars - V13 Ann Carol Crispin - Capcana paradisului 1.0 '{SF}.docx
Star Wars - V14 Ann Carol Crispin - Gambitul Hutt 1.0 '{SF}.docx
Star Wars - V15 Ann Carol Crispin - Zorii rebeliunii 1.0 '{SF}.docx
Star Wars - V16 Brian Daley - Han Solo pe Stars End 1.0 '{SF}.docx
Star Wars - V17 George Lucas - O noua speranta 2.1 '{SF}.docx
Star Wars - V18 Allan Dean Foster - Ochiul mintii 2.0 '{SF}.docx
Star Wars - V19 Donald F. Glut - Din aventurile lui Luke Skywalker 1.0 '{SF}.docx
Star Wars - V19 Donald F. Glut - Imperiul contraataca 2.1 '{SF}.docx
Star Wars - V20 Steve Perry - Umbrele imperiului 1.0 '{SF}.docx
Star Wars - V21 James Khan - Intoarcerea Cavalerului Jedi 2.1 '{SF}.docx
Star Wars - V22 Dave Wolverton - Cucerirea printesei Leia 1.0 '{SF}.docx
Star Wars - V23 Timothy Zahn - V1 Mostenitorul imperiului 1.1 '{SF}.docx
Star Wars - V24 Timothy Zahn - V2 Asaltul fortei intunecate 1.1 '{SF}.docx
Star Wars - V25 Timothy Zahn - Ultima porunca 1.1 '{SF}.docx
Star Wars - V26 Wonda M. McIntire - Steaua de cristal 1.2 '{SF}.docx
Star Wars - V27 Timothy Zahn - Spectre din trecut 1.2 '{SF}.docx
Star Wars - V28 Timothy Zahn - Viziuni din viitor 1.2 '{SF}.docx

./Staton Rabin:
Staton Rabin - Betsy si Napoleon 1.0 '{Tineret}.docx

./Stefana Cristina Czeller:
Stefana Cristina Czeller - Alta capra, alti trei iezi 0.99 '{SF}.docx
Stefana Cristina Czeller - Atinge-ma, Sara! 0.99 '{SF}.docx
Stefana Cristina Czeller - Ganganiile 0.99 '{SF}.docx
Stefana Cristina Czeller - Omul negru a venit 1.0 '{SF}.docx
Stefana Cristina Czeller - Patimile unui diavol de rangul trei 0.99 '{SF}.docx
Stefana Cristina Czeller - Petitorul 1.0 '{SF}.docx
Stefana Cristina Czeller - Razbunarea 0.99 '{SF}.docx
Stefana Cristina Czeller - Spovedania 0.99 '{SF}.docx
Stefana Cristina Czeller - Trambitele Apocalipsei sunat-au 0.99 '{SF}.docx
Stefana Cristina Czeller - Tristetea mesterului pietrar 0.99 '{SF}.docx

./Stefan Agopian:
Stefan Agopian - Tache de catifea 1.0 '{Diverse}.docx

./Stefan Ahnhem:
Stefan Ahnhem - Fabian Risk - V2 Al noualea mormant 1.0 '{Thriller}.docx
Stefan Ahnhem - Fabian Risk - V3 Minus optsprezece grade 1.0 '{Thriller}.docx
Stefan Ahnhem - Fabian Risk - V4 Motivul X 1.0 '{Thriller}.docx
Stefan Ahnhem - Victima fara chip 1.0 '{Politista}.docx

./Stefan Berciu:
Stefan Berciu - Contrabanda cu moartea 1.0 '{Politista}.docx
Stefan Berciu - Fantastica aventura 2101 3.0 '{SF}.docx
Stefan Berciu - Insula spionilor 2.0 '{Politista}.docx
Stefan Berciu - Intalnire in valea mortii 1.0 '{Politista}.docx
Stefan Berciu - Invidia 1.0 '{Politista}.docx
Stefan Berciu - Ipoteze judiciare 1.0 '{Politista}.docx
Stefan Berciu - Jos masca, domnule Dib 1.0 '{Politista}.docx
Stefan Berciu - Moartea unui singuratic 1.0 '{Politista}.docx
Stefan Berciu - Pichetul in alarma 1.0 '{Politista}.docx
Stefan Berciu - Pretul tacerii 2.0 '{Politista}.docx
Stefan Berciu - Pseudonimul 1.0 '{Politista}.docx
Stefan Berciu - Traficanti de carne vie 1.0 '{Politista}.docx
Stefan Berciu - Voalul negru 1.0 '{Politista}.docx

./Stefan Caraman:
Stefan Caraman - Morti si vii 0.9 '{Teatru}.docx
Stefan Caraman - Pianoman 0.99 '{Diverse}.docx

./Stefan Cazimir:
Stefan Cazimir - Amintiri despre Caragiale 0.9 '{Diverse}.docx

./Stefan Dumitrescu:
Stefan Dumitrescu - Delirul V2 1.0 '{Literatura}.docx

./Stefan Grabinski:
Stefan Grabinski - Linia moarta 0.99 '{SF}.docx

./Stefania Auci:
Stefania Auci - Leii Siciliei 1.0 '{Literatura}.docx

./Stefan Luca:
Stefan Luca - Nu e senina dimineata 1.0 '{Politista}.docx

./Stefan Octavian Iosif:
Stefan Octavian Iosif - Adio 1.0 '{Versuri}.docx
Stefan Octavian Iosif - Mi-e dor 1.0 '{Versuri}.docx
Stefan Octavian Iosif - Roze 1.0 '{Versuri}.docx

./Stefano Elio Danna:
Stefano Elio Danna - Scoala zeilor 0.9 '{Diverse}.docx

./Stefan Oprea:
Stefan Oprea - Procesul manechinelor 1.0 '{Politista}.docx

./Stefan Peca:
Stefan Peca - Picabo pinguini 0.9 '{Teatru}.docx

./Stefan Tita:
Stefan Tita - Alexida sensitiva 1.0 '{SF}.docx
Stefan Tita - Fluturele de ivoriu 1.0 '{ClubulTemerarilor}.docx
Stefan Tita - Gheizerul inghetat 1.0 '{ClubulTemerarilor}.docx
Stefan Tita - Robotul sentimental 1.0 '{SF}.docx

./Stefan Zaides:
Stefan Zaides - Paradisul celui care a sfidat lumea 0.99 '{SF}.docx

./Stefan Zeromski:
Stefan Zeromski - Ecourile padurii 1.0 '{Literatura}.docx

./Stefan Zweig:
Stefan Zweig - Casanova 1.0 '{AventuraIstorica}.docx
Stefan Zweig - Joseph Fouche 1.0 '{AventuraIstorica}.docx
Stefan Zweig - Jucatorul de sah 2.0 '{AventuraIstorica}.docx
Stefan Zweig - Lumea de ieri 1.0 '{AventuraIstorica}.docx
Stefan Zweig - Lupta in jurul unui rug 1.0 '{AventuraIstorica}.docx
Stefan Zweig - Magellan 1.0 '{AventuraIstorica}.docx
Stefan Zweig - Maria Antoaneta 1.0 '{AventuraIstorica}.docx
Stefan Zweig - Maria Stuart 1.0 '{AventuraIstorica}.docx
Stefan Zweig - Orele astrale ale omenirii 1.0 '{Istorie}.docx
Stefan Zweig - Secret arzator 1.0 '{AventuraIstorica}.docx
Stefan Zweig - Suflete zbuciumate 2.0 '{AventuraIstorica}.docx
Stefan Zweig - Triumful si destinul tragic al lui Erasm din Rotterdam 1.0 '{Biografie}.docx

./Stelian Fulga:
Stelian Fulga - A doua sansa 0.99 '{Sanatate}.docx

./Stelian Sarbu:
Stelian Sarbu - Negustorul de iluzii 1.0 '{Politista}.docx
Stelian Sarbu - Noaptea focurilor 1.0 '{Politista}.docx

./Stella Frances Nel:
Stella Frances Nel - Pasarea cantatoare 0.99 '{Dragoste}.docx

./Stel Pavlou:
Stel Pavlou - Codul Atlantidei 1.0 '{AventuraIstorica}.docx

./Stema Flint:
Stema Flint - Cer rosu in zori 0.9 '{Dragoste}.docx

./Stendhal:
Stendhal - Armance 1.0 '{ClasicSt}.docx
Stendhal - Manastirea din Parma V1 1.0 '{ClasicSt}.docx
Stendhal - Manastirea din Parma V2 1.0 '{ClasicSt}.docx
Stendhal - Rosu si negru V1 1.0 '{ClasicSt}.docx
Stendhal - Rosu si negru V2 1.0 '{ClasicSt}.docx

./Stephanie Garber:
Stephanie Garber - Caraval - V1 Caraval 1.0 '{SF}.docx
Stephanie Garber - Caraval - V2 Legendar 1.0 '{SF}.docx

./Stephanie Gertler:
Stephanie Gertler - In deriva 0.8 '{Literatura}.docx

./Stephanie James:
Stephanie James - In bratele unui necunoscut 1.0 '{Dragoste}.docx

./Stephanie Perkins:
Stephanie Perkins - V1 Anna si sarutul frantuzesc 1.0 '{Tineret}.docx
Stephanie Perkins - V2 Lola si baiatul de treaba din vecini 1.0 '{Tineret}.docx
Stephanie Perkins - V3 Isla si fericirea pana la adanci batraneti 1.0 '{Romance}.docx

./Stephen Baxter:
Stephen Baxter - Columbiadul 0.9 '{SF}.docx
Stephen Baxter - Corabiile timpului 1.0 '{CalatorieinTimp}.docx
Stephen Baxter - Mayflower V2 1.0 '{SF}.docx
Stephen Baxter - Misterul din Pacific 0.9 '{SF}.docx
Stephen Baxter - Pierdut in timp 1.0 '{CalatorieinTimp}.docx
Stephen Baxter - Planck Zero 0.99 '{SF}.docx

./Stephen Baxter & Eric Brown:
Stephen Baxter & Eric Brown - Putul spatio-temporal 0.99 '{CalatorieinTimp}.docx

./Stephen Booth:
Stephen Booth - Cainele negru 2.0 '{Politista}.docx
Stephen Booth - Salasul mortii 2.0 '{Politista}.docx

./Stephen Chbosky:
Stephen Chbosky - Jurnalul unui adolescent timid 1.0 '{Tineret}.docx

./Stephen Clarke:
Stephen Clarke - In ce merde te baga dragostea 1.0 '{Literatura}.docx
Stephen Clarke - In viata ai parte si de merde 0.99 '{Literatura}.docx
Stephen Clarke - Un an in merde 1.0 '{Literatura}.docx

./Stephen Coonts:
Stephen Coonts - Aripi '1.0 '{Razboi}.docx
Stephen Coonts - Cea de a saptesprezecea zi 1.0 '{SF}.docx

./Stephen Crane:
Stephen Crane - Asaltul 1.0 '{ActiuneRazboi}.docx

./Stephen Donaldson:
Stephen Donaldson - V1 Blestemul nobilului Foul 1.0 '{SF}.docx
Stephen Donaldson - V2 Razboiul uriasilor 1.0 '{SF}.docx
Stephen Donaldson - V3 Puterea salvatoare 1.0 '{SF}.docx

./Stephen Fry:
Stephen Fry - Mythos, Miturile Greciei repovestite 1.0 '{Tineret}.docx

./Stephen Hawking:
Stephen Hawking - Scurta istorie a timpului 0.99 '{MistersiStiinta}.docx
Stephen Hawking - Visul lui Einstein si alte eseuri 0.9 '{MistersiStiinta}.docx

./Stephen Hawking & Leonard Mlodinow:
Stephen Hawking & Leonard Mlodinow - Marele plan 2.0 '{MistersiStiinta}.docx

./Stephenie Meyer:
Stephenie Meyer - Amurg - V1 Amurg 2.0 '{Vampiri}.docx
Stephenie Meyer - Amurg - V2 Luna noua 2.1 '{Vampiri}.docx
Stephenie Meyer - Amurg - V3 Eclipsa 2.0 '{Vampiri}.docx
Stephenie Meyer - Amurg - V4 Zori de zi 2.0 '{Vampiri}.docx
Stephenie Meyer - Amurg - V5 Soarele de la miezul noptii 0.9 '{Vampiri}.docx
Stephenie Meyer - Chimista 1.0 '{Vampiri}.docx
Stephenie Meyer - Gazda 1.0 '{Vampiri}.docx
Stephenie Meyer - Noua si scurta viata a lui Bree Tanner 1.0 '{Vampiri}.docx

./Stephen Jones:
Stephen Jones - Cartea ororilor V1 1.0 '{AntologieHorror}.docx
Stephen Jones - Cartea ororilor V2 1.0 '{AntologieHorror}.docx

./Stephen Kelman:
Stephen Kelman - Si porumbeii vorbesc englezeste 0.9 '{Politista}.docx

./Stephen King:
Stephen King - Anotimpuri diferite 1.0 '{Horror}.docx
Stephen King - Apocalipsa 2.0 '{Horror}.docx
Stephen King - Blaze 2.0 '{Horror}.docx
Stephen King - Capcana pentru vise 1.0 '{Horror}.docx
Stephen King - Carrie 1.0 '{Horror}.docx
Stephen King - Casa intunericului 2.0 '{Horror}.docx
Stephen King - Ce-am gasit al meu sa fie 1.0 '{Horror}.docx
Stephen King - Christine 1.0 '{Horror}.docx
Stephen King - Cimitirul animalelor 1.0 '{Horror}.docx
Stephen King - Colorado Kid 1.0 '{Horror}.docx
Stephen King - Culoarul mortii 1.0 '{Horror}.docx
Stephen King - Dintr-un Buick 8 1.0 '{Horror}.docx
Stephen King - Doctor Sleep 1.0 '{Horror}.docx
Stephen King - Dolores Claiborne 1.0 '{Horror}.docx
Stephen King - Duma Key 1.0 '{Horror}.docx
Stephen King - Frumoasele adormite 1.0 '{Horror}.docx
Stephen King - Fugarul 2.0 '{Horror}.docx
Stephen King - Infruntarea 0.99 '{Horror}.docx
Stephen King - Institutul 1.0 '{Horror}.docx
Stephen King - J.F.K. 11.22.63 3.0 '{Horror}.docx
Stephen King - Jocul lui Gerald 1.0 '{Horror}.docx
Stephen King - Jumatatea intunecata 1.0 '{Horror}.docx
Stephen King - La asfintit 1.0 '{Horror}.docx
Stephen King - La miezul noptii V1 1.0 '{Horror}.docx
Stephen King - Lucruri pretioase 1.0 '{Horror}.docx
Stephen King - Lumea plajei 1.0 '{Horror}.docx
Stephen King - Marsul cel lung 1.0 '{Horror}.docx
Stephen King - Misery 1.0 '{Horror}.docx
Stephen King - Misterul regelui 1.0 '{Horror}.docx
Stephen King - Mobilul 2.0 '{Horror}.docx
Stephen King - Mr. Mercedes 1.0 '{Horror}.docx
Stephen King - Ochii dragonului 1.0 '{Horror}.docx
Stephen King - O mana de oase 1.0 '{Horror}.docx
Stephen King - Orasul bantuit V1 2.0 '{Horror}.docx
Stephen King - Orasul bantuit V2 2.0 '{Horror}.docx
Stephen King - Salem's lot 2.0 '{Horror}.docx
Stephen King - Santier in lucru 1.0 '{Horror}.docx
Stephen King - Shining 1.0 '{Horror}.docx
Stephen King - Strainul 1.0 '{Horror}.docx
Stephen King - Sub dom V1 1.0 '{Horror}.docx
Stephen King - Sub dom V2 1.0 '{Horror}.docx
Stephen King - The shining 1.0 '{Horror}.docx
Stephen King - Turnul Intunecat - V1 Pistolarul 2.0 '{Horror}.docx
Stephen King - Turnul Intunecat - V2 Alegerea celor trei 2.0 '{Horror}.docx
Stephen King - Turnul Intunecat - V3 Tinuturile pustii 2.0 '{Horror}.docx
Stephen King - Turnul Intunecat - V4.5 Vantul prin gaura cheii 1.0 '{Horror}.docx
Stephen King - Turnul Intunecat - V4 Vrajitorul si globul de cristal 2.0 '{Horror}.docx
Stephen King - Turnul Intunecat - V5 Lupii din Calla 3.0 '{Horror}.docx
Stephen King - Turnul Intunecat - V6 Cantecul lui Susannah 3.0 '{Horror}.docx
Stephen King - Turnul Intunecat - V7 Turnul intunecat 1.0 '{Horror}.docx
Stephen King - Zona moarta 1.0 '{Horror}.docx

./Stephen King & Peter Straub:
Stephen King & Peter Straub - Casa intunericului 2.0 '{Horror}.docx
Stephen King & Peter Straub - Talismanul 2.0 '{Horror}.docx

./Stephen LaBerge:
Stephen LaBerge - Explorarea viselor lucide 0.99 '{Psihologie}.docx

./Stephen R. George:
Stephen R. George - Durerea 1.0 '{Horror}.docx

./Steve Berry:
Steve Berry - Afacerea Columb 1.0 '{AventuraIstorica}.docx
Steve Berry - Al treilea secret 1.0 '{AventuraIstorica}.docx
Steve Berry - Camera de chihlimbar 1.0 '{AventuraIstorica}.docx
Steve Berry - Cotton Malone - V1 Mostenirea templierilor 1.0 '{AventuraIstorica}.docx
Steve Berry - Cotton Malone - V2 Conexiunea Alexandria 1.0 '{AventuraIstorica}.docx
Steve Berry - Cotton Malone - V3 Tradare la Venetia 1.0 '{AventuraIstorica}.docx
Steve Berry - Cotton Malone - V4 Tezaurul imparatului 1.0 '{AventuraIstorica}.docx
Steve Berry - Cotton Malone - V5 Razbunare la Paris 1.0 '{AventuraIstorica}.docx
Steve Berry - Cotton Malone - V7 Codul Jefferson 1.0 '{AventuraIstorica}.docx
Steve Berry - Cotton Malone - V9 Taina presedintelui 1.0 '{AventuraIstorica}.docx
Steve Berry - Enigma reginei 1.0 '{AventuraIstorica}.docx
Steve Berry - Profetia familiei Romanov 1.0 '{AventuraIstorica}.docx

./Steve Jackson:
Steve Jackson - Mentorul 1.0 '{Thriller}.docx

./Steven Erikson:
Steven Erikson - Cronicele Malazane - V1 Gradinile lunii 1.1 '{SF}.docx
Steven Erikson - Cronicele Malazane - V2 Portile casei mortilor 1.0 '{SF}.docx
Steven Erikson - Cronicele Malazane - V3 Amintirile ghetii 1.0 '{SF}.docx

./Steven Galloway:
Steven Galloway - Violoncelistul din Sarajevo 1.0 '{Literatura}.docx

./Steven Gould:
Steven Gould - Jumper 1.0 '{AventuraTineret}.docx
Steven Gould - Reflex 1.0 '{AventuraTineret}.docx

./Steven M. Greer:
Steven M. Greer - Adevarul ascuns. Informatii interzise 1.0 '{MistersiStiinta}.docx
Steven M. Greer - Contact extraterestru. Dovezi si consecinte V1 1.0 '{MistersiStiinta}.docx
Steven M. Greer - Contact extraterestru. Dovezi si consecinte V2 2.0 '{MistersiStiinta}.docx

./Steven Saylor:
Steven Saylor - Oameni si semizei V1 1.1 '{AventuraIstorica}.docx
Steven Saylor - Oameni si semizei V2 1.1 '{AventuraIstorica}.docx
Steven Saylor - O crima pe Via Appia 1.1 '{AventuraIstorica}.docx
Steven Saylor - Valul profetiilor 1.1 '{AventuraIstorica}.docx

./Steven Sora:
Steven Sora - Comoara pierduta a Cavalerilor Templieri 0.7 '{Civilizatii}.docx

./Steven Spielberg:
Steven Spielberg - Intalnire de gradul 3 0.6 '{SF}.docx

./Steve Perry:
Steve Perry - Riposta 1.0 '{SF}.docx

./Stieg Larsson:
Stieg Larsson - Millenium - V1 Barbati care urasc femeile 3.0 '{Thriller}.docx
Stieg Larsson - Millenium - V2 Fata care s-a jucat cu focul 3.0 '{Thriller}.docx
Stieg Larsson - Millenium - V3 Castelul din nori s-a sfaramat 3.0 '{Thriller}.docx
Stieg Larsson - Millenium - V4 Prizoniera in panza de paianjen 1.0 '{Thriller}.docx
Stieg Larsson - Millenium - V5 Dinte pentru dinte 1.0 '{Thriller}.docx

./Stoica Ludescu:
Stoica Ludescu - Letopisetul Cantacuzinesc 0.9 '{Istorie}.docx

./Stona Fitch:
Stona Fitch - Fara simturi 1.0 '{Literatura}.docx

./Strabon:
Strabon - Geografia V1 0.9 '{Istorie}.docx

./Stratis Mirivillis:
Stratis Mirivillis - Invatatoarea cu ochi de aur 1.0 '{Dragoste}.docx

./Stuart MacBride:
Stuart MacBride - Granitul rece 1.0 '{Thriller}.docx

./Suad:
Suad - Arsa de vie 1.0 '{Thriller}.docx

./Sue Grafton:
Sue Grafton - A de la alibi 1.0 '{Politista}.docx
Sue Grafton - B de la bandit 1.0 '{Politista}.docx
Sue Grafton - C de la cadavru 1.0 '{Politista}.docx
Sue Grafton - D de la datornic 1.0 '{Politista}.docx

./Sue Monk Kidd:
Sue Monk Kidd - Invata sa ai aripi 1.0 '{Literatura}.docx
Sue Monk Kidd - Jiltul sirenei 1.0 '{Literatura}.docx

./Sun Tzu:
Sun Tzu - Arta razboiului 1.0 '{Istorie}.docx

./Susana Fortes:
Susana Fortes - Septembrie poate astepta 1.0 '{Literatura}.docx

./Susan Barrie:
Susan Barrie - Ca intr un vis 0.7 '{Romance}.docx
Susan Barrie - Inventatorul indragostit 0.99 '{Romance}.docx
Susan Barrie - Patimile inimii 1.0 '{Romance}.docx

./Susan Dennard:
Susan Dennard - Taramul Vrajitorilor - V1 Vrajitoarea adevarului 1.0 '{Supranatural}.docx
Susan Dennard - Taramul Vrajitorilor - V2 Vrajitorul vantului 1.0 '{Supranatural}.docx

./Susan Doyle:
Susan Doyle - Aniversare buclucasa 0.99 '{Dragoste}.docx
Susan Doyle - Dragoste si fidelitate 0.99 '{Dragoste}.docx
Susan Doyle - Dulce amar melodia curgea 0.99 '{Dragoste}.docx
Susan Doyle - In plasa seductiei 0.99 '{Dragoste}.docx
Susan Doyle - In sfarsit acasa 0.9 '{Romance}.docx
Susan Doyle - Intoarcere in timp 0.99 '{Dragoste}.docx
Susan Doyle - Mai bine mai tarziu 0.99 '{Dragoste}.docx
Susan Doyle - Nu ma casatoresc cu tine 0.99 '{Dragoste}.docx
Susan Doyle - Tot ce imi doresc 0.8 '{Dragoste}.docx

./Susan Ee:
Susan Ee - Penryn si Sfarsitul Lumii - V1 Ingeri cazuti 1.0 '{SF}.docx
Susan Ee - Penryn si Sfarsitul Lumii - V2 Lumea de apoi 1.0 '{SF}.docx
Susan Ee - Penryn si Sfarsitul Lumii - V3 Sfarsitul lumii 1.0 '{SF}.docx

./Susan Gayle:
Susan Gayle - Fata in albastru 0.99 '{Romance}.docx

./Susan Johnson:
Susan Johnson - Blaze dragostea mea 0.5 '{Romance}.docx
Susan Johnson - Ingerii pacatelor 1.0 '{Romance}.docx
Susan Johnson - Insula scoicilor 0.99 '{Dragoste}.docx
Susan Johnson - Jocul de puzzle 0.9 '{Dragoste}.docx
Susan Johnson - Pacate candide 1.0 '{Erotic}.docx
Susan Johnson - Un iubit fantastic 1.0 '{Dragoste}.docx

./Susan Krinard:
Susan Krinard - Castelul viselor 0.99 '{Dragoste}.docx

./Susanna Clarke:
Susanna Clarke - Doamnele din Grace Adieu 1.0 '{SF}.docx
Susanna Clarke - Jonathan Strange 0.99 '{Supranatural}.docx

./Susannah Appelbaum:
Susannah Appelbaum - Otravurile din Caux - V1 Nestemata scobita 0.99 '{SF}.docx
Susannah Appelbaum - Otravurile din Caux - V2 Breasla degustatorilor 0.99 '{SF}.docx

./Susanna Tamaro:
Susanna Tamaro - Asculta glasul meu 1.0 '{Literatura}.docx
Susanna Tamaro - Mergi unde te poarta inima 1.0 '{Diverse}.docx

./Susanne Dunlap:
Susanne Dunlap - Sarutul lui Liszt 1.0 '{Aventura}.docx

./Susanne Jansson:
Susanne Jansson - Mlastina 1.0 '{Literatura}.docx

./Susanne McCarty:
Susanne McCarty - Un seducator versat 0.99 '{Romance}.docx

./Susan Runde:
Susan Runde - Nimic nu ne va desparti 0.99 '{Dragoste}.docx

./Susan Smith:
Susan Smith - Schimb de locuri 0.99 '{Romance}.docx
Susan Smith - Vis Texan 0.99 '{Dragoste}.docx

./Sushila Blackman:
Sushila Blackman - Cum trec in nefiinta marile personalitati 1.0 '{MistersiStiinta}.docx

./Su Tong:
Su Tong - Lumea de orez 1.0 '{Literatura}.docx

./Suzanne Ashley:
Suzanne Ashley - Pandora si magicianul 1.0 '{Romance}.docx

./Suzanne Brockmann:
Suzanne Brockmann - Limbajul trupului 0.9 '{Dragoste}.docx

./Suzanne Collins:
Suzanne Collins - Jocurile Foamei - V1 Jocurile foamei 4.1 '{Aventura}.docx
Suzanne Collins - Jocurile Foamei - V2 Sfidarea 4.1 '{Aventura}.docx
Suzanne Collins - Jocurile Foamei - V3 Revolta 4.1 '{Aventura}.docx

./Suzanne Dye:
Suzanne Dye - Arsita tropicala 0.9 '{Dragoste}.docx
Suzanne Dye - Barbatul din luna de miere 0.99 '{Dragoste}.docx
Suzanne Dye - Cand destinul se joaca 0.9 '{Romance}.docx
Suzanne Dye - Cu ajutorul matusii 0.9 '{Dragoste}.docx
Suzanne Dye - Povara amintirilor 0.99 '{Dragoste}.docx
Suzanne Dye - Uraganul 0.99 '{Romance}.docx
Suzanne Dye - Viata in doi 0.99 '{Dragoste}.docx

./Suzanne Guntrum:
Suzanne Guntrum - Nasterea unei pasiuni 0.99 '{Romance}.docx

./Suzanne Mcminn:
Suzanne Mcminn - Anotimpul singuratic 0.9 '{Dragoste}.docx
Suzanne Mcminn - Durerea unei iubiri 0.9 '{Dragoste}.docx
Suzanne Mcminn - Strainul din apartament 0.9 '{Romance}.docx
Suzanne Mcminn - Un dar nepretuit 0.9 '{Dragoste}.docx

./Sven Hassel:
Sven Hassel - V01 - 1953 Legiunea blestematilor 2.0 '{ActiuneRazboi}.docx
Sven Hassel - V02 - 1958 Blindatele mortii 2.0 '{ActiuneRazboi}.docx
Sven Hassel - V03 - 1960 Camarazi de front 2.0 '{ActiuneRazboi}.docx
Sven Hassel - V04 - 1962 Batalion de mars 2.0 '{ActiuneRazboi}.docx
Sven Hassel - V05 - 1963 Gestapo 2.0 '{ActiuneRazboi}.docx
Sven Hassel - V06 - 1963 Monte Cassino 2.0 '{ActiuneRazboi}.docx
Sven Hassel - V07 - 1967 Lichidati Parisul 2.0 '{ActiuneRazboi}.docx
Sven Hassel - V08 - 1969 General SS 2.0 '{ActiuneRazboi}.docx
Sven Hassel - V09 - 1971 Imperiul iadului 2.0 '{ActiuneRazboi}.docx
Sven Hassel - V10 - 1976 Moarte si viscol 2.0 '{ActiuneRazboi}.docx
Sven Hassel - V11 - 1977 Drum sangeros catre moarte 2.0 '{ActiuneRazboi}.docx
Sven Hassel - V12 - 1979 Curtea martiala 2.0 '{ActiuneRazboi}.docx
Sven Hassel - V13 - 1981 Inchisoarea OGPU 2.0 '{ActiuneRazboi}.docx
Sven Hassel - V14 - 1985 Comisarul 2.0 '{ActiuneRazboi}.docx

./Sven Vesting:
Sven Vesting - Vraja Milioanelor - V1 Cap. 1-56 1.0 '{IstoricaSt}.docx
Sven Vesting - Vraja milioanelor - V2 Cap. 57-102 1.0 '{IstoricaSt}.docx
Sven Vesting - Vraja milioanelor - V3 Cap. 103-159 1.0 '{IstoricaSt}.docx
Sven Vesting - Vraja milioanelor - V4 Cap. 160-211 1.0 '{IstoricaSt}.docx
Sven Vesting - Vraja milioanelor - V5 Cap. 212-261 1.0 '{IstoricaSt}.docx
Sven Vesting - Vraja Milioanelor - V6 Cap. 262-305 1.0 '{IstoricaSt}.docx
Sven Vesting - Vraja Milioanelor - V7 Cap. 306-327 1.0 '{IstoricaSt}.docx
Sven Vesting - Vraja milioanelor 1.0 '{IstoricaSt}.docx

./Swami Sivananda:
Swami Sivananda - Puterea gandului 1.0 '{Spiritualitate}.docx

./Swami Vishnu Devananda:
Swami Vishnu Devananda - Descoperirea si disciplinarea fortei mentale 0.99 '{Spiritualitate}.docx

./Swen Hasel:
Swen Hasel - Batalia de la Ardeni 0.99 '{ActiuneRazboi}.docx
Swen Hasel - Desertul damnatilor 1.0 '{ActiuneRazboi}.docx
Swen Hasel - Nisipurile insangerate 0.99 '{ActiuneRazboi}.docx
Swen Hasel - Suflete ciuruite 1.0 '{ActiuneRazboi}.docx
Swen Hasel - Tornada de foc 0.9 '{ActiuneRazboi}.docx

./Sylvain Neuvel:
Sylvain Neuvel - Gigantii adormiti 1.0 '{SF}.docx

./Sylvain Reynard:
Sylvain Reynard - Infernul lui Gabriel - V1 Infernul lui Gabriel 1.0 '{Romance}.docx
Sylvain Reynard - Infernul lui Gabriel - V2 Extazul lui Gabriel 0.9 '{Romance}.docx
Sylvain Reynard - Infernul lui Gabriel - V3 Izbavirea lui Gabriel 1.0 '{Romance}.docx

./Sylvia Bishop:
Sylvia Bishop - Mexicanu 0.99 '{Romance}.docx

./Sylvia Day:
Sylvia Day - Crossfire - V1 Atractia 1.0 '{Romance}.docx
Sylvia Day - Crossfire - V2 Revelatia 1.0 '{Romance}.docx
Sylvia Day - Crossfire - V3 Implinirea 1.0 '{Romance}.docx
Sylvia Day - Crossfire - V4 Fascinatia 0.99 '{Romance}.docx

./Sylvia Nasar:
Sylvia Nasar - O minte sclipitoare 1.0 '{Literatura}.docx

./Sylvie Germain:
Sylvie Germain - Cartea noptilor 1.0 '{Literatura}.docx
Sylvie Germain - Intamplari dintr-o viata 1.0 '{Literatura}.docx
Sylvie Germain - Zile de manie 1.0 '{Literatura}.docx

./Szilvasi Lajos:
Szilvasi Lajos - Sete de aer 2.0 '{Dragoste}.docx
```

